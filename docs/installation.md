Install with npm

```html static
npm install component-lib --save
```

Install with yarn

```html static
yarn add component-lib
```