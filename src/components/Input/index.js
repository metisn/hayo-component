import styled from '@emotion/styled'
import PropTypes from 'prop-types'

const InputComponent = styled.button`
  label: Demo;
  text-align: center;
  font-size: 20px;
  background-color: red;
  border-radius: 6px;
  border: none;
`

const Input = ({ name }) => {
  return <InputComponent>{name}</InputComponent>
}

Input.propTypes = {
  name: PropTypes.string.isRequired,
}

Input.defaultProps = {
  name: 'Demo',
}

export default Input
