import styled from '@emotion/styled'
import PropTypes from 'prop-types'

const ButtonComponent = styled.button`
  label: Demo;
  text-align: center;
  font-size: 20px;
  background-color: pink;
  border-radius: 50%;
  border: none;
  height: 36px;
`

const Button = ({ name }) => {
  return <ButtonComponent>{name}</ButtonComponent>
}

Button.propTypes = {
  name: PropTypes.string.isRequired,
}

Button.defaultProps = {
  name: 'Demo',
}

export default Button
